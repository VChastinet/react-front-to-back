import React, { Component } from 'react';

import { Consumer } from '../../context';
import TextInputGroup from '../layout/TextInputGroup';
import axios from 'axios';

const defaultStateValues = {
  formType: 'Add',
  name: '',
  email: '',
  phone: '',
  errors: {}
}

class AddContact extends Component {
  state = {
    initialForm: 'Add',
    ...defaultStateValues
  };

  async componentDidMount() {
    const { match } = this.props;

    if (match.params.id) {
      const res = await axios.get(
        `https://jsonplaceholder.typicode.com/users/${match.params.id}`
      );
      const contact = res.data;
      this.setState({
        name: contact.name,
        email: contact.email,
        phone: contact.phone
      });
    }
  }

  static getDerivedStateFromProps(props, state) {
    const { match } = props;
    const { initialForm, formType } = state;

    if (match.params.id) {
      return { formType: 'Edit' };
    } else if (initialForm !== formType) {
      return defaultStateValues;
    } else {
      return { formType: 'Add' };
    }
  }

  handleSubmit = async (dispatch, event) => {
    event.preventDefault();
    const { email, name, phone } = this.state;
    const { history, match } = this.props;

    if (!name) {
      this.setState({ errors: { name: 'Name field is required' } });
      return;
    }

    if (!email) {
      this.setState({ errors: { email: 'Email field is required' } });
      return;
    }

    if (!phone) {
      this.setState({ errors: { phone: 'Phone field is required' } });
      return;
    }

    const contactInfo = {
      email,
      name,
      phone
    };

    if (!match.params.id) {
      const request = await axios.post(
        'https://jsonplaceholder.typicode.com/users/',
        contactInfo
      );
      dispatch({ type: 'ADD_CONTACT', payload: request.data });
    } else {
      const request = await axios.put(
        `https://jsonplaceholder.typicode.com/users/${match.params.id}`,
        contactInfo
      );
      dispatch({ type: 'UPDATE_CONTACT', payload: request.data });
    }

    this.setState(defaultStateValues);

    history.push('/');
  };

  handleChange = (event) => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };

  render() {
    const { email, name, phone, errors, formType } = this.state;
    return (
      <Consumer>
        {({ dispatch }) => (
          <div className="card mb-3">
            <div className="card-header">
              <span>{formType} Contact</span>
            </div>
            <div className="card-body">
              <form onSubmit={this.handleSubmit.bind(this, dispatch)}>
                <TextInputGroup
                  onChange={this.handleChange}
                  label="name"
                  name="name"
                  placeholder="Enter name"
                  value={name}
                  error={errors.name}
                />
                <TextInputGroup
                  onChange={this.handleChange}
                  label="email"
                  name="email"
                  placeholder="Enter email"
                  type="email"
                  value={email}
                  error={errors.email}
                />
                <TextInputGroup
                  onChange={this.handleChange}
                  label="phone"
                  name="phone"
                  placeholder="Enter phone"
                  value={phone}
                  error={errors.phone}
                />
                <input
                  type="submit"
                  value={`${formType} Contact`}
                  className="btn btn-outline-success btn-block"
                />
              </form>
            </div>
          </div>
        )}
      </Consumer>
    );
  }
}

export default AddContact;
