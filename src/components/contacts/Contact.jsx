import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Consumer } from '../../context';
import axios from 'axios';

class Contact extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showContactInfo: false
    };
  }

  handleSort = () => {
    this.setState({ showContactInfo: !this.state.showContactInfo });
  };

  handleDelete = async (id, dispatch) => {
    await axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`)
    dispatch({type: 'DELETE_CONTACT', payload: id});
  };

  render() {
    const { name, email, phone, id } = this.props;
    const { showContactInfo } = this.state;
    const icon = showContactInfo ? 'fa-sort-up' : 'fa-sort-down';

    return (
      <Consumer>
        {({dispatch}) => (
          <div className="card card-body mb-3">
            <h4>
              {name}
              <i onClick={this.handleSort} className={`ml-2 fas ${icon}`} style={{ cursor: 'pointer' }} />
              <i onClick={this.handleDelete.bind(this, id, dispatch)} className="fas fa-times text-danger float-right" style={{ cursor: 'pointer' }} />
              <Link to={`contact/edit/${id}`}>
                <i className="fas fa-pencil-alt text-primary" style={{
                  cursor: 'pointer',
                  float: 'right',
                  marginRight: '1rem'
                }} />
              </Link>
            </h4>
            {showContactInfo && (
              <ul className="list-group">
                <li className="list-group-item">{email}</li>
                <li className="list-group-item">{phone}</li>
              </ul>
            )}
          </div>
        )}
      </Consumer>
    );
  }
}

Contact.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired
};

export default Contact;
