import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const TextInputGroup = ({
  label,
  name,
  value,
  placeholder,
  onChange,
  type,
  error
}) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input
        className={classnames('form-control form-control-lg', {
          'is-invalid': !!error
        })}
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
      />
      {!!error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

TextInputGroup.prototype = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  error: PropTypes.string
};

TextInputGroup.defaultProps = {
  placeholder: 'Input',
  type: 'text',
  error: ''
};
export default TextInputGroup;
